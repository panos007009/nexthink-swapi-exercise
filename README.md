# Dockerized SWAPI application in Spring as part of the Junior Software Engineer Quiz of Nexthink
A simple Spring Boot app that leverages the data provided by SWAPI (https://swapi.dev/) and returns the data transformed as requested by the interview´s specifications.

## Running the application

1. Open a terminal and navigate to the application folder
2. Type `docker build -t swapiapp .` and after the image is built type `docker run -p 6969:8080 swapiapp`

## Exposed Endpoints (By running the image mapped to port 6969)

1. GET http://localhost:6969/api/starships/{characterName}

Returns the names of the starships a character has flown with.<br>
Example of response for "Darth Vader":

    [
    {
    "name": "TIE Advanced x1",
    "model": "Twin Ion Engine Advanced x1",
    "manufacturer": "Sienar Fleet Systems",
    "cost_in_credits": "unknown",
    "length": "9.2",
    "max_atmosphering_speed": "1200",
    "crew": "1",
    "passengers": "0",
    "cargo_capacity": "150",
    "consumables": "5 days",
    "hyperdrive_rating": "1.0",
    "MGLT": "105",
    "starship_class": "Starfighter",
    "pilots": [
    "http://swapi.dev/api/people/4/"
    ],
    "films": [
    "http://swapi.dev/api/films/1/"
    ],
    "created": "2014-12-12T11:21:32.991000Z",
    "edited": "2014-12-20T21:23:49.889000Z",
    "url": "http://swapi.dev/api/starships/13/"
    }
    ]

2. GET http://localhost:6969/api/inhabitants/{planetName}

Returns the names of the characters that inhabit the specified planet.<br>
Example of response for "Tatooine":

    [
    "Luke Skywalker",
    "C-3PO",
    "Darth Vader",
    "Owen Lars",
    "Beru Whitesun lars",
    "R5-D4",
    "Biggs Darklighter",
    "Anakin Skywalker",
    "Shmi Skywalker",
    "Cliegg Lars"
    ]

3. Includes a commented out GET request, identical to the second one that changes the response to a proper JSON of the characters instead of just the characters names

