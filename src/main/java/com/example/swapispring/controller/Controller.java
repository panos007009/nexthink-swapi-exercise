package com.example.swapispring.controller;

import com.example.swapispring.entity.responseOfInhabitantsFromPlanetName.InitialResponseOfPlanets;
import com.example.swapispring.entity.responseOfInhabitantsFromPlanetName.Planet;
import com.example.swapispring.entity.responseOfInhabitantsFromPlanetName.Residents;
import com.example.swapispring.entity.responseOfStarshipsFromCharacterName.Character;
import com.example.swapispring.entity.responseOfStarshipsFromCharacterName.InitialResponseOfCharacters;
import com.example.swapispring.entity.responseOfStarshipsFromCharacterName.Starship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private RestTemplate restTemplate;

    //Endpoint to return JSON of starships that the given character has piloted
    @RequestMapping("/starships/{characterName}")
    public ArrayList<Starship> getStarships(@PathVariable("characterName") String characterName) {

        //Rest Template get for initial response JSON (The character)
        InitialResponseOfCharacters initialResponseOfCharacters = restTemplate.getForObject("https://swapi.dev/api/people/?search=" + characterName, InitialResponseOfCharacters.class);

        //Array list of starships to be returned as the final response
        ArrayList<Starship> results = new ArrayList<>();
        assert initialResponseOfCharacters != null;
        if (initialResponseOfCharacters.getCharacters().size() == 1) {
            for (Character tempCharacter : initialResponseOfCharacters.getCharacters()) {
                for (String tempStarship : tempCharacter.getStarships()) {
                    tempStarship = tempStarship.replaceAll("http", "https");
                    Starship resultingObject = restTemplate.getForObject(tempStarship, Starship.class);
                    results.add(resultingObject);
                }
            }
            //error handling
        } else {
            Starship starshipError = new Starship();
            starshipError.setAdditionalProperty("Error", "More than one or no characters found please refine your search");
            results.add(starshipError);
        }
        return results;
    }

    //Endpoint to return the list of names that live in the given planet
    @RequestMapping("/inhabitants/{planetName}")
    public ArrayList<String> getResidents(@PathVariable("planetName") String planetName) {

        //Rest Template get for initial response JSON (THe character)
        InitialResponseOfPlanets initialResponseOfPlanets = restTemplate.getForObject("https://swapi.dev/api/planets/?search=" + planetName, InitialResponseOfPlanets.class);

        //Array list of starships to be returned as the final response
        ArrayList<String> results = new ArrayList<>();

        assert initialResponseOfPlanets != null;
        if (initialResponseOfPlanets.getPlanets().size() == 1) {
            for (Planet tempPlanet : initialResponseOfPlanets.getPlanets()) {
                for (String tempResidents : tempPlanet.getResidents()) {
                    tempResidents = tempResidents.replaceAll("http", "https");
                    Residents resultingObject = restTemplate.getForObject(tempResidents, Residents.class);
                    assert resultingObject != null;
                    results.add(resultingObject.getName());
                }
            }
            //error handling
        } else {
            String planetError = "Error : More than one or no planets found please refine your search";
            results.add(planetError);
        }
        return results;

    }

    //Optional function to return the full JSON response of the residents not only the names

//    @RequestMapping("/inhabitants/{planetName}")
//    public ArrayList<Residents> getResidents(@PathVariable("planetName") String planetName) {
//
//        //Rest Template get for initial response JSON (THe character)
//        InitialResponseOfPlanets initialResponseOfPlanets = restTemplate.getForObject("https://swapi.dev/api/planets/?search=" + planetName, InitialResponseOfPlanets.class);
//
//        //Array list of starships to be returned as the final response
//        ArrayList<Residents> results = new ArrayList<>();
//
//        if (initialResponseOfPlanets.getPlanets().size() == 1) {
//            for (Planet tempPlanet : initialResponseOfPlanets.getPlanets()) {
//                for (String tempResidents : tempPlanet.getResidents()) {
//                    tempResidents = tempResidents.replaceAll("http", "https");
//                    Residents resultingObject = restTemplate.getForObject(tempResidents, Residents.class);
//                    results.add(resultingObject);
//                }
//            }
//            ;
//            //error handling
//        } else {
//            Residents error = new Residents();
//            error.setAdditionalProperty("Error", "More than one planets found please refine your search");
//            results.add(error);
//        }
//        return results;
//    }

}
