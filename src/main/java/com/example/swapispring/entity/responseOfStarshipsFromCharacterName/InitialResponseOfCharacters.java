package com.example.swapispring.entity.responseOfStarshipsFromCharacterName;

import com.fasterxml.jackson.annotation.*;
import org.json.JSONObject;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "next",
        "previous",
        "results"
})
@Generated("jsonschema2pojo")
public class InitialResponseOfCharacters extends JSONObject {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("next")
    private Object next;
    @JsonProperty("previous")
    private Object previous;
    @JsonProperty("results")
    private List<Character> results = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public InitialResponseOfCharacters() {
    }

    /**
     *
     * @param next
     * @param previous
     * @param count
     * @param results
     */
    public InitialResponseOfCharacters(Integer count, Object next, Object previous, List<Character> results) {
        super();
        this.count = count;
        this.next = next;
        this.previous = previous;
        this.results = results;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public InitialResponseOfCharacters withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("next")
    public Object getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(Object next) {
        this.next = next;
    }

    public InitialResponseOfCharacters withNext(Object next) {
        this.next = next;
        return this;
    }

    @JsonProperty("previous")
    public Object getPrevious() {
        return previous;
    }

    @JsonProperty("previous")
    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public InitialResponseOfCharacters withPrevious(Object previous) {
        this.previous = previous;
        return this;
    }

    @JsonProperty("results")
    public List<Character> getCharacters() {
        return results;
    }

    @JsonProperty("results")
    public void setCharacters(List<Character> results) {
        this.results = results;
    }

    public InitialResponseOfCharacters withCharacters(List<Character> results) {
        this.results = results;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public InitialResponseOfCharacters withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
