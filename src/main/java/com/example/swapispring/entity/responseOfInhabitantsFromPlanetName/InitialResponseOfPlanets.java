package com.example.swapispring.entity.responseOfInhabitantsFromPlanetName;

import com.fasterxml.jackson.annotation.*;
import org.json.JSONObject;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "next",
        "previous",
        "results"
})
@Generated("jsonschema2pojo")
public class InitialResponseOfPlanets extends JSONObject {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("next")
    private Object next;
    @JsonProperty("previous")
    private Object previous;
    @JsonProperty("results")
    private List<Planet> results = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public InitialResponseOfPlanets() {
    }

    /**
     *
     * @param next
     * @param previous
     * @param count
     * @param results
     */
    public InitialResponseOfPlanets(Integer count, Object next, Object previous, List<Planet> results) {
        super();
        this.count = count;
        this.next = next;
        this.previous = previous;
        this.results = results;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public InitialResponseOfPlanets withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("next")
    public Object getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(Object next) {
        this.next = next;
    }

    public InitialResponseOfPlanets withNext(Object next) {
        this.next = next;
        return this;
    }

    @JsonProperty("previous")
    public Object getPrevious() {
        return previous;
    }

    @JsonProperty("previous")
    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public InitialResponseOfPlanets withPrevious(Object previous) {
        this.previous = previous;
        return this;
    }

    @JsonProperty("results")
    public List<Planet> getPlanets() {
        return results;
    }

    @JsonProperty("results")
    public void setPlanets(List<Planet> results) {
        this.results = results;
    }

    public InitialResponseOfPlanets withPlanets(List<Planet> results) {
        this.results = results;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public InitialResponseOfPlanets withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
